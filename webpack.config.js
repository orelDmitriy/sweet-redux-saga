module.exports = () => {
  return {
    entry: ['./src/index.js'],
    mode: 'production',
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
        },
      ],
    },
  };
};
