import { connect as connectRedux } from 'react-redux';

export function connect(...props) {
  return function(constructor) {
       return connectRedux(...props)(constructor);
  };
}
