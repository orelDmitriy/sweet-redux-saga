export function payload(...params) {
  return function(target, propertyKey, descriptor) {
    if (typeof descriptor.value !== 'function') {
      descriptor.initializer = () => new ConfigurePayload(params);
    }
    return descriptor;
  };
}

export function actionsCreator() {
  return function(constructor) {
    const instance = new constructor();
    const fields = Object.getOwnPropertyNames(instance);

    return class extends constructor {
      constructor() {
        super();

        fields.forEach(fieldName => {
          const fieldValue = instance[fieldName];
          if (!(fieldValue instanceof ConfigurePayload) && fieldValue) {
            this[fieldName] = fieldValue;
            return;
          }
          const actionType = createActionTypeByFieldName(fieldName);

          this[fieldName] = createSimpleActionCreator(fieldValue, actionType);

          this[fieldName].actionType = actionType;
          this[fieldName].toString = () => actionType;
          this[fieldName].toPrimitive = () => actionType;
          this[fieldName].valueOf = () => actionType;
        });
      }
    };
  };
}

function createActionTypeByFieldName(methodName) {
  return methodName.replace(/([A-Z])/g, '_$1').toUpperCase();
}

function createSimpleActionCreator(fieldConfig, actionType) {
  return (...args) => {
    let payload = createPayloadByFieldConfig(fieldConfig, args);
    if (!payload && args.length) {
      payload = args;
    }

    return {
      type: actionType,
      payload,
    };
  };
}

function createPayloadByFieldConfig(fieldValue, args) {
  return (
    fieldValue &&
    Array.isArray(fieldValue.payloadFields) &&
    fieldValue.payloadFields.reduce((accumulator, current, index) => {
      accumulator[current] = args && args[index];
      return accumulator;
    }, {})
  );
}

class ConfigurePayload {
  constructor(props) {
    this.payloadFields = props;
  }
}
