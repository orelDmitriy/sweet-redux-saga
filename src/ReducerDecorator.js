export function reducer(initialState = {}) {
  return function(constructor) {
    const instance = new constructor();

    return class {
      constructor() {
        return function(state = initialState, action) {
          if (!action) {
            return state;
          }

          const reducer = instance[action.type];
          if (reducer && typeof reducer === 'function') {
            return reducer(state, action.payload);
          }

          return state;
        };
      }
    };
  };
}
