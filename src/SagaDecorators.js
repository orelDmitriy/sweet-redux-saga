import * as effects from 'redux-saga/effects';

export function sagas() {
  return function(constructor) {
    const watchers = (constructor.prototype._watchers || []).map(watcher => {
      return effects.fork(constructor.prototype[watcher]);
    });

    return class {
      constructor() {
        return (function*() {
          yield effects.all(watchers);
        })();
      }
    };
  };
}

export function filterActions(filterFunc) {
  return function(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function*(action, ...props) {
      const state = yield effects.select();
      const shouldCall = yield filterFunc({ state, ...action });
      if (!shouldCall) {
        return;
      }

      yield originalMethod(action, ...props);
    };

    return descriptor;
  };
}

export function catchError(catchFunc) {
  return function(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function*(action, ...props) {
      try {
        yield originalMethod(action, ...props);
      } catch (error) {
        const state = yield effects.select();
        yield catchFunc({ error, state, ...action });
      }
    };

    return descriptor;
  };
}

export function takeEvery(pattern) {
  return overrideOrigin(effects.takeEvery, pattern);
}

export function takeLatest(pattern) {
  return overrideOrigin(effects.takeLatest, pattern);
}

function overrideOrigin(effect, pattern) {
  return function(target, propertyKey, descriptor) {
    const originalMethod = descriptor.value;

    target._watchers = target._watchers || [];
    target._watchers.push(propertyKey);

    descriptor.value = function*() {
      function* withTryCatch(action, ...props) {
        try {
          yield originalMethod(action, ...props);
        } catch (error) {
          console.error(error);
        }
      }
      yield effect([pattern], withTryCatch);
    };

    return descriptor;
  };
}
