import { actionsCreator, payload } from '../src';

describe('Actions creator', () => {
  let mockFn;
  let testActions;

  const objectProps = {
    field_1: 123,
    id: 44332,
  };

  const simpleProp = 123;
  const anotherProps = 'some string for example';

  beforeAll(() => {
    mockFn = jest.fn();

    @actionsCreator()
    class TestActions {
      myAction;

      @payload()
      actionWithEmptyPayload;

      @payload('firstParam', 'secondParam')
      actionWithNamedParams;

      simpleFields = 10;

      @payload()
      fieldWithValue = 10;

      @payload('param')
      methodWithPayload() {
        mockFn();
      }

      method() {
        mockFn();
      }
    }

    testActions = new TestActions();
  });

  it('fields and methods', () => {
    expect(testActions.simpleFields).toEqual(10);

    testActions.method();
    expect(mockFn).toBeCalledTimes(1);

    testActions.methodWithPayload();
    expect(mockFn).toBeCalledTimes(2);
  });

  it('create simple actions', () => {
    expect(testActions.myAction()).toEqual({
      type: 'MY_ACTION',
      payload: undefined,
    });

    expect(testActions.myAction(objectProps, simpleProp)).toEqual({
      type: 'MY_ACTION',
      payload: [objectProps, simpleProp],
    });
  });

  it('get action type', () => {
    expect(testActions.myAction.toString()).toBe('MY_ACTION');
    expect(testActions.myAction.toPrimitive()).toBe('MY_ACTION');
    expect(testActions.myAction.valueOf()).toBe('MY_ACTION');
  });

  it('passe params by payload', () => {
    expect(testActions.actionWithEmptyPayload(objectProps, simpleProp)).toEqual(
      {
        type: 'ACTION_WITH_EMPTY_PAYLOAD',
        payload: {},
      }
    );

    expect(testActions.actionWithNamedParams(objectProps, simpleProp)).toEqual({
      type: 'ACTION_WITH_NAMED_PARAMS',
      payload: {
        firstParam: objectProps,
        secondParam: simpleProp,
      },
    });

    expect(testActions.actionWithNamedParams(objectProps)).toEqual({
      type: 'ACTION_WITH_NAMED_PARAMS',
      payload: {
        firstParam: objectProps,
        secondParam: undefined,
      },
    });

    expect(
      testActions.actionWithNamedParams(simpleProp, objectProps, anotherProps)
    ).toEqual({
      type: 'ACTION_WITH_NAMED_PARAMS',
      payload: {
        firstParam: simpleProp,
        secondParam: objectProps,
      },
    });

    expect(testActions.fieldWithValue(anotherProps)).toEqual({
      type: 'FIELD_WITH_VALUE',
      payload: {},
    });
  });
});
