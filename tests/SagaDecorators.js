import { all, put, delay } from 'redux-saga/effects';
import SagaTester from 'redux-saga-tester';
import { filterActions, sagas, takeEvery, takeLatest } from '../src';

describe('Saga', () => {
  let firstMockFn;
  let secondMockFn;
  let lastMockFn;

  const initialState = { root: 'init state' };
  let sagaTester = null;

  beforeEach(() => {
    firstMockFn = jest.fn();
    secondMockFn = jest.fn();
    lastMockFn = jest.fn();

    @sagas()
    class OurSaga {
      *simpleSaga() {
        firstMockFn();
      }

      @takeEvery('SOME_ACTIONS')
      *someAction() {
        secondMockFn();
      }

      @takeEvery('FEW_ACTIONS')
      *fewAction() {
        yield delay(10);
        firstMockFn();
        yield put({ type: 'SOME_ACTIONS' });
      }

      @takeLatest('LATEST_CHECK')
      *latestAction() {
        yield delay(10);
        firstMockFn();
        yield put({ type: 'SOME_ACTIONS' });
      }

      @takeEvery(['FILTER_ACTIONS'])
      @filterActions(({ state, type, payload }) => {
        return (
          state.root === initialState.root &&
          type === 'FILTER_ACTIONS' &&
          payload.data === 13
        );
      })
      *filterActionsSaga({ payload }) {
        firstMockFn(payload.data);
      }

      @takeEvery(['FIRST_ACTION', 'SECOND_ACTION'])
      *checkCallFewDifferentActions() {
        firstMockFn();
      }

      @takeEvery(['*'])
      *callForAllActions() {
        lastMockFn();
      }
    }

    const rootSagas = function*() {
      yield all([new OurSaga()]);
    };

    sagaTester = new SagaTester({ initialState });
    sagaTester.start(rootSagas);
  });

  it('call saga', async () => {
    sagaTester.dispatch({ type: 'ANY_ACTION' });
    sagaTester.dispatch({ type: 'SOME_ACTIONS' });
    await sagaTester.waitFor('SOME_ACTIONS');
    expect(firstMockFn).toBeCalledTimes(0);
    expect(secondMockFn).toBeCalledTimes(1);
  });

  it('takeEvery', async () => {
    sagaTester.dispatch({ type: 'FEW_ACTIONS' });
    sagaTester.dispatch({ type: 'FEW_ACTIONS' });
    await sagaTester.waitFor('SOME_ACTIONS');
    expect(firstMockFn).toBeCalledTimes(2);
  });

  it('takeLatest', async () => {
    sagaTester.dispatch({ type: 'LATEST_CHECK' });
    sagaTester.dispatch({ type: 'LATEST_CHECK' });
    await sagaTester.waitFor('SOME_ACTIONS');
    expect(firstMockFn).toBeCalledTimes(1);
  });

  it('filterActions', async () => {
    sagaTester.dispatch({ type: 'FILTER_ACTIONS', payload: { data: 14 } });
    await sagaTester.waitFor('FILTER_ACTIONS');
    expect(firstMockFn).toBeCalledTimes(0);

    sagaTester.dispatch({ type: 'FILTER_ACTIONS', payload: { data: 13 } });
    await sagaTester.waitFor('FILTER_ACTIONS');
    expect(firstMockFn).toBeCalledTimes(1);
    expect(firstMockFn).toBeCalledWith(13);
  });

  it('call few different action', async () => {
    sagaTester.dispatch({ type: 'FIRST_ACTION' });
    sagaTester.dispatch({ type: 'SECOND_ACTION' });
    await sagaTester.waitFor('SECOND_ACTION');
    expect(firstMockFn).toBeCalledTimes(2);
  });

  it('call for all different action', async () => {
    sagaTester.dispatch({ type: 'FIRST_ACTION_FOR_ALL' });
    sagaTester.dispatch({ type: 'SECOND_ACTION_FOR_ALL' });
    sagaTester.dispatch({ type: 'LAST_ACTIONS_FOR_ALL' });
    await sagaTester.waitFor('LAST_ACTIONS_FOR_ALL');
    expect(lastMockFn).toBeCalledTimes(3);
  });
});
