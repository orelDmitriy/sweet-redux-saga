import { reducer } from '../src';

describe('Reducers', () => {
  let testReducer;
  let initialState = {
    first: 'first',
    last: null,
  };

  const unknownAction = {
    type: 'UNKNOWN_ACTION',
  };

  const payloadText = 'text';
  const someAction = {
    type: 'SOME_ACTIONS',
    payload: {
      text: payloadText,
    },
  };

  beforeAll(() => {
    @reducer(initialState)
    class TestReducer {
      ['SOME_ACTIONS'](state, payload) {
        return {
          ...state,
          text: payload.text,
        };
      }
    }

    testReducer = new TestReducer();
  });

  it('pass unknown action', () => {
    const resultSate = testReducer(initialState, unknownAction);
    expect(resultSate).toEqual(initialState);
  });

  it('process known action', () => {
    const resultSate = testReducer(initialState, someAction);
    expect(resultSate).toEqual({ ...initialState, text: payloadText });
  });
});
